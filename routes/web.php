<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@login');

Route::group(['middleware' => ['auth','CheckRole:siswa']], function () {


    Route::get('/siswa', 'mainController@siswa');
    Route::get('/getMapel', 'mainController@getDataMapel');

    Route::get('/getNilai/{nis}', 'mainController@getDataNilai');
    Route::get('/display', 'mainController@getDataSiswaAll');
    Route::get('/display/{kelas}/{jurusan}', 'mainController@getDatasiswa');
});

Route::group(['middleware' => ['auth','CheckRole:guru']], function () {
    Route::get('index/guru/lihatnilai/{nip}', 'GuruController@lihatnilai');

    Route::get('index/guru/formnilai', 'GuruController@formnilai')->name('form_nilai_siswa');

    Route::get('index/guru/getNisSiswa/', 'GuruController@getNisSiswa');

    Route::get('/index/guru/getMapel', 'GuruController@getMapel');

    Route::get('/index/guru/{nis}/formEditNilai', 'GuruController@formEditNilai');

    Route::post('index/guru/create_nilai', 'GuruController@createnilai')->name('create_nilai_siswa');
    
});

Route::group(['middleware' => ['auth','CheckRole:siswa,guru,admin']], function () {
    Route::get('/index', function () {
        return view('index');
    })->name('index');


});


Route::group(['middleware' => 'auth','CheckRole:admin'], function () {


    Route::post('/index', 'AdminController@create')->name('index_post');

    Route::get('/index/tambahdata','AdminController@tambahdata')->name('admin_tambahdata');

    Route::get('/index/data_prodi/{prodi}','AdminController@getProdi');

    Route::post('index/lihatdata/{user}', 'AdminController@lihatdata')->name('admin_lihatdatauser');

    Route::get('index/lihatdata', function () {
        return view('admin_layouts.lihatdata');
    })->name('admin_lihatdata');

    Route::get('/index/{user}/editdata', 'AdminController@edit')->name('admin_editdataform');

    Route::delete('index/{user}', 'AdminController@delete');

    Route::patch('index/{user}','AdminController@update');

    Route::get('/index/getProdi1', 'AdminController@getProdi1')->name('getProdi1');
});

Route::post('/postlogin', 'AuthController@postlogin');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'AuthController@logout')->name('logout');







