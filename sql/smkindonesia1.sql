-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2019 at 07:51 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smkindonesia1`
--

-- --------------------------------------------------------

--
-- Table structure for table `kelas_siswa`
--

CREATE TABLE `kelas_siswa` (
  `idks` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_guru`
--

CREATE TABLE `tabel_guru` (
  `nip_guru` varchar(255) NOT NULL,
  `nama_guru` varchar(30) NOT NULL,
  `kelas_guru` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_guru`
--

INSERT INTO `tabel_guru` (`nip_guru`, `nama_guru`, `kelas_guru`) VALUES
('02930192031', 'Daniel Yessayas', 'Multimedia,Teknik Otomasi Industri'),
('10170076850', 'Marcyano Chelsea', 'Rekayasa Perangkat Lunak,Teknik Konstruksi Kayu');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kelas`
--

CREATE TABLE `tabel_kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(15) NOT NULL,
  `id_prodi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_kelas`
--

INSERT INTO `tabel_kelas` (`id_kelas`, `nama_kelas`, `id_prodi`) VALUES
(5, 'X RPL 2', 2),
(7, 'X RPL 1', 2),
(8, 'X DPIB 2', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_mapel`
--

CREATE TABLE `tabel_mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_mapel`
--

INSERT INTO `tabel_mapel` (`id_mapel`, `nama_mapel`) VALUES
(1, 'Matematika'),
(2, 'Agama'),
(3, 'PJOK'),
(4, 'Bahasa Indonesia'),
(5, 'Bahasa Jepang');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_mapelguru`
--

CREATE TABLE `tabel_mapelguru` (
  `idmg` int(11) NOT NULL,
  `kdmapel` int(11) NOT NULL,
  `nis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_mengajar`
--

CREATE TABLE `tabel_mengajar` (
  `idmr` int(11) NOT NULL,
  `idmg` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_nilai`
--

CREATE TABLE `tabel_nilai` (
  `id_nilai` int(11) NOT NULL,
  `nis_siswa` varchar(255) NOT NULL,
  `nip_guru` varchar(255) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `uh` int(11) NOT NULL,
  `uts` int(11) NOT NULL,
  `uas` int(11) NOT NULL,
  `na` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_nilai`
--

INSERT INTO `tabel_nilai` (`id_nilai`, `nis_siswa`, `nip_guru`, `id_mapel`, `uh`, `uts`, `uas`, `na`) VALUES
(1, '10170076691', '10170076850', 2, 50, 78, 85, 68);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_prodi`
--

CREATE TABLE `tabel_prodi` (
  `id_prodi` int(11) NOT NULL,
  `nama_prodi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_prodi`
--

INSERT INTO `tabel_prodi` (`id_prodi`, `nama_prodi`) VALUES
(1, 'Multimedia'),
(2, 'Rekayasa Perangkat Lunak'),
(3, 'Desain Permodelan dan Informasi'),
(4, 'Bisnis Konstruksi & Properti'),
(5, 'Teknik Komputer Jaringan'),
(6, 'Teknik Otomasi Industri'),
(7, 'Teknik Kendaraan Ringan'),
(8, 'Teknik Konstruksi Kayu'),
(9, 'Teknik Fabrikasi Logam dan Manufakturing'),
(10, 'Sistem Informasi Jaringan');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_siswa`
--

CREATE TABLE `tabel_siswa` (
  `nis_siswa` varchar(255) NOT NULL,
  `nama_siswa` varchar(30) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_prodi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_siswa`
--

INSERT INTO `tabel_siswa` (`nis_siswa`, `nama_siswa`, `id_kelas`, `id_prodi`) VALUES
('10170076691', 'Afghan Eka P', 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nis` varchar(255) CHARACTER SET latin1 NOT NULL,
  `role` enum('admin','siswa','guru','') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `nis`, `role`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'Adminstrator', '1', 'admin', 'agan@email.com', NULL, '$2y$12$V0NjyAHRRwWMq8z8MObCMOZsXWm/bOuchv2eoWYD5FyM18f9gKtaa', '3WUWE7trKOSWU7YNwbm3CCEE79mMxUrqqHHDtACySiq9cO6S5lpZlfIRtGwE', '2019-10-20 02:39:13', '2019-10-20 04:33:31'),
(46, 'Marcyano Chelsea', '10170076850', 'guru', 'marchyanochelsea9@gmail.com', NULL, '$2y$10$SG70OpG9Y24jfFyIeoW6P.pIMAd9Vn63HomJaa/rTvwV1nHO4mjRm', 'sJYESXkUnZcX9cVV7d6auiccYkI0BSa0cknYwhm14ewm1mi9IXXLZGxsHDAE', '2019-10-21 00:21:25', '2019-10-21 00:21:25'),
(47, 'Afghan Eka P', '10170076691', 'siswa', 'afghanekapangestu@gmail.com', NULL, '$2y$10$2WyjgoJPn.NqTUzqY27Re.NiSJST4N/ErElSnKWIYAv60ysYVVLHC', 'kukqXiPpsEe1cgJfohrJHfy8JyhmI0605Ua40qJYUfqLWw4i9iJwyb3KCE8d', '2019-10-21 06:53:04', '2019-10-21 06:53:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kelas_siswa`
--
ALTER TABLE `kelas_siswa`
  ADD PRIMARY KEY (`idks`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_guru`
--
ALTER TABLE `tabel_guru`
  ADD PRIMARY KEY (`nip_guru`);

--
-- Indexes for table `tabel_kelas`
--
ALTER TABLE `tabel_kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `id_prodi` (`id_prodi`);

--
-- Indexes for table `tabel_mapel`
--
ALTER TABLE `tabel_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tabel_mapelguru`
--
ALTER TABLE `tabel_mapelguru`
  ADD PRIMARY KEY (`idmg`);

--
-- Indexes for table `tabel_mengajar`
--
ALTER TABLE `tabel_mengajar`
  ADD PRIMARY KEY (`idmr`);

--
-- Indexes for table `tabel_nilai`
--
ALTER TABLE `tabel_nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `nis_siswa` (`nis_siswa`),
  ADD KEY `id_mapel` (`id_mapel`),
  ADD KEY `nip_guru` (`nip_guru`);

--
-- Indexes for table `tabel_prodi`
--
ALTER TABLE `tabel_prodi`
  ADD PRIMARY KEY (`id_prodi`);

--
-- Indexes for table `tabel_siswa`
--
ALTER TABLE `tabel_siswa`
  ADD PRIMARY KEY (`nis_siswa`),
  ADD UNIQUE KEY `id_kelas` (`id_kelas`,`id_prodi`) USING BTREE,
  ADD UNIQUE KEY `id_prodi` (`id_prodi`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nis` (`nis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_kelas`
--
ALTER TABLE `tabel_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tabel_mapel`
--
ALTER TABLE `tabel_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tabel_mapelguru`
--
ALTER TABLE `tabel_mapelguru`
  MODIFY `idmg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_nilai`
--
ALTER TABLE `tabel_nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tabel_prodi`
--
ALTER TABLE `tabel_prodi`
  MODIFY `id_prodi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tabel_kelas`
--
ALTER TABLE `tabel_kelas`
  ADD CONSTRAINT `tabel_kelas_ibfk_1` FOREIGN KEY (`id_prodi`) REFERENCES `tabel_prodi` (`id_prodi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tabel_nilai`
--
ALTER TABLE `tabel_nilai`
  ADD CONSTRAINT `tabel_nilai_ibfk_2` FOREIGN KEY (`id_mapel`) REFERENCES `tabel_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tabel_nilai_ibfk_3` FOREIGN KEY (`nis_siswa`) REFERENCES `tabel_siswa` (`nis_siswa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tabel_nilai_ibfk_4` FOREIGN KEY (`nip_guru`) REFERENCES `tabel_guru` (`nip_guru`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tabel_siswa`
--
ALTER TABLE `tabel_siswa`
  ADD CONSTRAINT `tabel_siswa_ibfk_1` FOREIGN KEY (`id_prodi`) REFERENCES `tabel_prodi` (`id_prodi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tabel_siswa_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `tabel_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
