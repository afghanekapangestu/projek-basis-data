@extends('index')

@section('title','Edit Data | Admin | ')

@section('edit_tambahdata')
<div class="panel-heading">
    <h3>Form Edit Data</h3>
</div>

<div class="panel-body">
    @if ($errors->any())
        <div class="alert alert-warning">
            <h4>Pesan Error</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <form action="../{{ $data[0]->id }}" method="POST">
            <div class="col-md-6">
                <label for="nis">Identitas</label>
                @method('patch')
                @csrf
                <input class="form-control input-lg" placeholder="Identitas" name="nis" type="text" value="{{ $data[0]->nis }}"><br>
                <label for="name">Nama</label>
                <input class="form-control input-lg" placeholder="Nama" name="name" type="text" value="{{ $data[0]->name }}"><br>
                <label for="name">Role</label>
                <select name="role" id="role" class="form-control role">
                    <option value="{{ $data[0]->role }}" hidden selected>{{ $data[0]->role }}</option>
                    <option value="admin">Admin</option>
                    <option value="guru">Guru</option>
                    <option value="siswa">Siswa</option>
                </select><br>

                <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim</button>
                <button type="reset" class="btn btn-danger"><i class="fa fa-ban"></i> Reset</button>
            </div>

            <div class="col-md-6">
                <label for="password">Password</label>
                <input class="form-control input-lg" placeholder="Password" name="password" type="text" value="{{ $data[0]->password }}"><br>
                <label for="email">Email</label>
                <input class="form-control input-lg " placeholder="Email" name="email" type="email" value="{{ $data[0]->email }}"><br>
                <div class="divkosong">

                </div>
            </div>

            
    </div>
</div>
</form>
</div>
@endsection

@section('scriptjs')
    <script>
        $(".role").change(function(){
            var a = $(this).children("option:selected").val();
            $(".divkosong").html("");

            if (a==="siswa"){
                $('.divkosong').append(`
                 <label for="id_kelas">Kelas</label>
                    <select name='id_kelas' id="id_kelas" class="form-control">
                        <option hidden selected>--Pilih--</option>
                        @foreach ($data as $item)
                            <option value="{{ $item->id_kelas }}">{{ $item->nama_kelas }}</option>
                        @endforeach
                    </select>
                    <input type='hidden' id="id_prodi" name="id_prodi" value="">
                `);

            id_kelas();
            }else if( a==="guru"){
                $(".divkosong").append(`
                    <label for="kelas_guru">
                        Kelas Guru
                    </label>
                    <div id="inputCheckbox">

                    </div>
                `);
                kelas_guru();
            }
        });

        function kelas_guru(){

            $("#inputCheckbox").html("");

            $.ajax({
                type : "GET",
                url : '{{route("getProdi1")}}',
                success : function(response){
                    console.log(response);
                    $.each(response,function(i,v){
                        $("#inputCheckbox").append(`
                            <input type="checkbox" value="`+response[i].nama_prodi+`" name="kelas_guru[]">&nbsp;` +response[i].nama_prodi+`<br>
                        `);
                    });

                },
                error:function(response){
                    console.log(response);
                }
            });
        }

        function id_kelas(){
            $("#id_kelas").change(function(){
            var c = $(this).children("option:selected").val();
            $.ajax({
                type: 'GET',
                url : '{{ URL::to("/index/data_prodi") }}/'+c,
                success : function(response){
                    console.log(response);
                    $("#id_prodi").val(response[0].id_prodi);
                },
                error:function(xhr){
                    console.log(xhr);
                }
            });
        });
        }

    </script>
@endsection
