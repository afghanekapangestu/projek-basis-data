@extends('index')

@section('title','Lihat Data | Admin | ')

@section('lihatdata')
    <div class="row mt-2">
        <div class="col-md-12">
                @if ( $message = Session::get('status'))
                <div class="alert alert-success">
                    <ul>
                        <li>{{ $message }}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-6">
            <select name="role" id="selrole" class="form-control">
                <option value="" selected hidden>--Pilih--</option>
                <option value="admin">Admin</option>
                <option value="guru">Guru</option>
                <option value="siswa">Siswa</option>
            </select>
        </div>
        <div class="col-md-6">
            <select name="state" id="maxRows" class="form-control">
                <option value="0" selected hidden id="pilih">--Pilih--</option>
                <option value="5">5</option>
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>
        <div class="col-md-12">
             <table class="table table-hover striped table-lihatdata">
                 <thead>
                    <th>Nama</th>
                    <th>Nomor Identitas</th>
                    <th>Role</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Remember Token</th>
                    <th>Action</th>
                 </thead>


                <tbody id="bodyIsiDataAdmin">

                </tbody>
            </table>

            <div class="pagination-container">
                <nav>
                    <ul class="pagination">

                    </ul>
                </nav>
            </div>
        </div>
    </div>


@endsection
