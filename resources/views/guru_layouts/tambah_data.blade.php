@extends('index')

@section('title','Tambah Nilai Siswa | Guru |')

@section('tambah_nilai_siswa')
<div class="panel-heading">
    <h3>Tambah Nilai Siswa</h3>
</div>

<div class="panel-body">
    @if ($errors->any())
        <div class="alert alert-warning">
            <h4>Pesan Error</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <form action="{{ route('create_nilai_siswa') }}" method="POST">
        @csrf
        <div class="col-md-4">
            <label for="nis_siswa">Nis Siswa</label>
            <input type="number" class="form-control showModal" name="nis_siswa" id="nis_siswa"><br>

        </div>
        <div class="col-md-4">
            <label for="nip_guru">Nip Guru</label>
            <input type="number" class="form-control" name="nip_guru" readonly value="{{auth()->user()->nis}}"><br>
        </div>
        <div class="col-md-4">
            <label for="id_mapel">Mapel</label>
            <select name="id_mapel" id="id_mapel" class="form-control">
            
            </select><br>
        </div>
        <div class="col-md-3">
            <label for="uh">Ulangan Harian</label>
            <input type="number" class="form-control inputtest" name="uh" id="inputUH"  >
        </div>
        <div class="col-md-3">
            <label for="uts">Ulangan Tengah Semester</label>
            <input type="number" class="form-control inputtest" name="uts" id="inputUts" >
        </div>
        <div class="col-md-3">
            <label for="uas">Ulangan Akhir Semester</label>
            <input type="number" class="form-control inputtest" name="uas" id="inputUas" >
        </div>
        <div class="col-md-3">
            <label for="na">Total</label>
            <input type="number" class="form-control " name="na" readonly id="inputNA"><br>
        </div>

        <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim</button>
        <button type="reset" class="btn btn-danger"><i class="fa fa-ban"></i> Reset</button>
    </div>
</div>
</form>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Data</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scriptjs')
    <script>
        $(".showModal").click(function(){
            $(".modal").modal("show")
            var getName = $(this).attr("name")
            $(".modal-body").html("");
            if(getName === "nis_siswa"){
                $(".modal-body").append(`

                <table class="table tabel-striped table-data">
                    <thead id="theadIsiData">
                        <th>#</th>
                        <th>Nis Siswa</th>
                        <th>Nama Siswa</th>
                        <th>Nama Kelas</th>
                        <th>Nama Prodi</th>
                    </thead>
                    <tbody id="tbodyIsiData">

                    </tbody>
                </table>
                                
                   `)                            
                dataklik(getName)

                $.ajax({
                    type : 'GET',
                    url : '{{URL::to("index/guru/getNisSiswa/")}}',
                    success: function(response){
                        var ol = 1;
                        var oli = 0;
                        $.each(response,function(i,v){


                            $("#tbodyIsiData").append(`
                                <tr>
                                    <td>`+ ol++ +`</td>
                                    <td>`+response[i].nis_siswa+`</td>
                                    <td>`+response[i].nama_siswa+`</td>
                                    <td>`+response[i].nama_kelas+`</td>
                                    <td>`+response[i].nama_prodi+`</td>
                                </tr>
                            `)
                        })
                    }

                });
            }
        });

        function dataklik(getName){
            $('.table-data tbody').on('click','tr',function(){
                var currow = $(this).closest('tr')
                var col1 = currow.find('td:eq(1)').text()
                $("#nis_siswa").val(col1);
                $(".modal").modal("hide")
            });
        }

        $(document).ready(function(){
                $.ajax({
                    type : 'GET',
                    url : '{{URL::to("/index/guru/getMapel")}}',
                    success:function(response){
                        $.each(response,function(i,value){
                                $("#id_mapel").append(`
                                        <option value="`+response[i].id_mapel+`">`+response[i].nama_mapel+`</option>
                                `)
                            });

                    }
                });
            })

            $(".inputtest").click(function(){
                $("#inputUH,#inputUts,#inputUas").keyup(function(){
                    var uh = parseInt($("#inputUH").val())
                    var uts = parseInt($("#inputUts").val())
                    var uas = parseInt($("#inputUas").val())
                    var total = (uh+uts+uas)/3
                    $("#inputNA").val(total.toFixed(1));


                })
            })

            function handleChange(input) {
                if (input.value < 0) input.value = 0;
                if (input.value > 100) input.value = 100;
            }
             


    </script>
@endsection
