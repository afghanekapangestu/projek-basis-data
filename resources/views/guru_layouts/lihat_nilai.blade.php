@extends('index')

@section('title','Lihat Nilai | Guru | ')

@section('lihat_nilaiguru')
<div class="panel-heading">
    <h3>Lihat Data</h3>
</div>

<div class="panel-body">
    @if ($errors->any())
        <div class="alert alert-warning">
            <h4>Pesan Error</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    


    <div class="row">
             <div class="col-md-12">
                @if ( $message = Session::get('status'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ $message }}</li>
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                        <th>Nis Siswa</th>
                        <th>Nip Guru</th>
                        <th>UH</th>
                        <th>UTS</th>
                        <th>UAS</th>
                        <th>NA</th>
                        <th>Action</th>
                        
                    </thead>
                    
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->nis_siswa }}</td>
                            <td>{{ $item->nip_guru }}</td>
                            <td>{{ $item->uh }}</td>
                            <td>{{ $item->uts }}</td>
                            <td>{{ $item->uas }}</td>
                            <td>{{ $item->na }}</td>
                            <td>
                                <a href="#" class="btn btn-primary">Edit</a>
                                <button class="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
    </div>
</div>
</form>
</div>
@endsection
