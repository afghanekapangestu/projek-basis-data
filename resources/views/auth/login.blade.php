{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{asset('css/materialize.min.css')}}">
    <title>Halaman Login</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h2 class="header">Halaman Login</h2>
                <div class="card horizontal">
                  <div class="card-image">
                  </div>
                  <div class="card-stacked">
                        <div class="card-content">

                            <form action="{{URL::to('/postlogin')}}" method="POST">
                                <div class="input-field col s12">
                                        @csrf
                                        <label for="password">Nomor Identitas</label><br>
                                        <input id="nis" type="text" class="validate" name="nis">

                                </div>
                                <div class="input-field col s12">
                                        <label for="password">Password</label><br>
                                        <input  id="password" type="password" class="validate" name="password">
                                </div>
                        </div>
                    <div class="card-action">
                            <button class="waves-effect waves-light btn" type="submit">Login</button>

                    </form>
                    </div>
                  </div>
                </div>
              </div>

        </div>
    </div>
</body>
</html> --}}


<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login | Klorofil - Free Bootstrap Dashboard Template</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('/vendor/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('/vendor/linearicons/style.css')}}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{asset('/css/main.css')}}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{asset('/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('/img/favicon.png')}}">

    <style>
    .auth-box .right {
        float: right;
        width: 58%;
        height: 100%;
        position: relative;
        background-image: url("{{asset('/img/login-bg.jpg')}}");
        background-repeat: no-repeat;
        background-size: cover;
    }
    </style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="{{asset('/img/Untitled.png')}}" alt="Klorofil Logo"></div>
								<p class="lead">Login to your account</p>
							</div>
                            <form action="{{URL::to('/postlogin')}}" method="POST">
                                @csrf
								<div class="form-group">
									<label for="signin-identitas" class="control-label sr-only">identitas</label>
									<input type="text" class="form-control" id="signin-identitas" name="nis"  placeholder="Nomor Identitas">
								</div>
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" class="form-control" id="signin-password" name="password" placeholder="Password">
								</div>
								{{-- <div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox">
										<span>Remember me</span>
									</label>
								</div> --}}
								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
								{{-- <div class="bottom">
									<span class="helper-text"><i class="fa fa-lock"></i> <a href="#">Forgot password?</a></span>
								</div> --}}
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">SMK INDONESIA - Basis Data</h1>
							<p>by Afghan Eka Pangestu</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
