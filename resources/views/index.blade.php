
<!doctype html>
<html lang="en">

<head>
	<title>@yield('title') - SMKINDONESIA</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{asset('/vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('/vendor/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('/vendor/linearicons/style.css')}}">
	<link rel="stylesheet" href="{{asset('/vendor/chartist/css/chartist-custom.css')}}">
	<!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('/css/main.css')}}">
	<!-- DataTables -->



    {{-- Meta Content for CSRF --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{asset('/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('/img/favicon.png')}}">

    <style>
        .table-lihatdata{
            table-layout: fixed;
        }
        .table-lihatdata td{
            overflow: hidden;
        }

        input[type='number'] {
            -moz-appearance:textfield;
        }
    </style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="{{ route('index') }}"><img src="{{asset('/img/brand.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<form class="navbar-form navbar-left">
					<div class="input-group">
						<input type="text" value="" class="form-control" placeholder="Search dashboard...">
						<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
					</div>
				</form>

				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
								<i class="lnr lnr-alarm"></i>
								<span class="badge bg-danger">5</span>
							</a>
							<ul class="dropdown-menu notifications">
								<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>System space is almost full</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9 unfinished tasks</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly report is available</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly meeting in 1 hour</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your request has been approved</a></li>
								<li><a href="#" class="more">See all notifications</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('/img/user.png')}}" class="img-circle" alt="Avatar"> <span>{{auth()->user()->name}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">

                                <li><a href="{{URL::to('logout')}}"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
						<!-- <li>
							<a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
						</li> -->
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">

                        @if (auth()->user()->role=="admin")
                            <li>
                                <a href="{{ route('index') }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                    <a href="#subPages1" data-toggle="collapse" class="collapsed"><i class="lnr lnr-plus-circle"></i> <span>Tambah Data</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                    <div id="subPages1" class="collapse ">
                                        <ul class="nav">
                                            <li>
                                                    <a href="{{ route('admin_tambahdata') }}"><i class="lnr lnr-chevron-right"></i> User</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            <li>
                                    <a href="#subPages2" data-toggle="collapse" class="collapsed"><i class="lnr lnr-dice"></i> <span>Lihat Data</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                    <div id="subPages2" class="collapse ">
                                        <ul class="nav">
                                            <li>
                                                    <a href="{{ route('admin_lihatdata') }}"><i class="lnr lnr-chevron-right"></i> User</a>
                                            </li>
                                            <li>
                                                    <a href="#"><i class="lnr lnr-chevron-right"></i> Kelas</a>
                                            </li>
                                            <li>
                                                    <a href="#"><i class="lnr lnr-chevron-right"></i> Mapel</a>
                                            </li>
                                            <li>
                                                    <a href="#"><i class="lnr lnr-chevron-right"></i> Prodi</a>
                                            </li>
                                            <li>
                                                    <a href="#"><i class="lnr lnr-chevron-right"></i> Nilai</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                        @elseif(auth()->user()->role=="guru")
                                <li>
                                    <a href="{{ route('index') }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('form_nilai_siswa') }}"><i class="lnr lnr-plus-circle"></i>Tambah Nilai Siswa</a>
                                </li>
                                <li>
                                <a href="{{URL::to('index/guru/lihatnilai/')}}/{{auth()->user()->nis}}"><i class="lnr lnr-dice"></i>Lihat Nilai Siswa</a>
                                </li>
                        @endif

					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
                    <div class="panel">

                        @if (Request::path() == 'index/tambahdata' and auth()->user()->role=="admin" )
                            @yield('admin_tambahdata')
                        @elseif(Request::path() == 'index' and auth()->user()->role=="admin")
                            hai
                        @elseif(Request::path() == 'index/lihatdata' and auth()->user()->role=="admin")
                            @yield('lihatdata')
                        @elseif(Request::is('index/*/editdata') and auth()->user()->role=="admin")
                            @yield('edit_tambahdata')
                        @elseif(Request::path() == 'index/guru/formnilai' and auth()->user()->role=="guru")
                            @yield('tambah_nilai_siswa')
                        
                        @elseif(Request::is('index/guru/lihatnilai/*') and auth()->user()->role=="guru")
                            @yield('lihat_nilaiguru')
                        
                        @endif

                    </div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">Shared by <i class="fa fa-love"></i><a href="https://www.afghan.id">AfghanID</a>
</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
	<script src="{{asset('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{asset('/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
	<script src="{{asset('/vendor/chartist/js/chartist.min.js')}}"></script>
    <script src="{{asset('/scripts/klorofil-common.js')}}"></script>
	<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });





        var selected;
        var table = ".table-lihatdata";
        $("#maxRows,#selrole").on('change',function(){
            $(".pagination").html('');

            var trnum = 0;
            var maxRows = parseInt($(this).val());
            var totalRows = $(table+' tbody tr').length;
            $(table+' tr:gt(0)').each(function(){
                trnum++;
                if(trnum > maxRows){
                    $(this).hide();
                }
                if(trnum <= maxRows){
                    $(this).show();
                }
            });
            if(totalRows > maxRows){
                var pagenum = Math.ceil(totalRows/maxRows);
                for (var i=1;i<=pagenum;){
                    $('.pagination').append('<li data-page="'+i+'">\<span>'+ i++ +'<span class="sr-only">(current)</span></span>\</li>').show();
                }
            }
            $(".pagination li:first-child").addClass('active');
            $('.pagination li').on('click',function(){
                var pageNum = $(this).attr('data-page');
                var trIndex = 0;
                $('.pagination li').removeClass("active");
                $(this).addClass("active");
                $(table+ ' tr:gt(0)').each(function(){
                    trIndex++;
                    if(trIndex > (maxRows*pageNum) || trIndex <= ((maxRows*pageNum)-maxRows)){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
            });
        });
        $(function(){
            $('table tr:eq(0)').prepend('<th>#</th>');
            var id = 0;
            $('table tr:gt(0)').each(function(){
                id++;
                $(this).prepend('<td>'+id+'</td>');
            });
        });


        $("#selrole").change(function(){
                selected = $(this).children("option:selected").val();

                $("#maxRows").val(0);
                $(".pagination").html("");
                // console.log(selected);
                $("#bodyIsiDataAdmin").html("");
                jQuery.ajax({
                type : 'POST',
                url : '{{URL::to("index/lihatdata")}}/'+selected,
                success: function(response){
                    var id = 0;
                    if(!$.trim(response)){
                        $("#bodyIsiDataAdmin").append(`
                            <tr>
                                <td colspan="8" align="center"> Data Tidak Ada</td>
                            </tr>
                        `);
                    }
                    $.each(response,function(i,v){
                        id++;
                        $("#bodyIsiDataAdmin").append(`
                            <tr>
                                <td>
                                    `+id+`
                                </td>
                                <td>
                                    `+response[i].name+`
                                </td>
                                <td>
                                    `+response[i].nis+`
                                </td>
                                <td>
                                    `+response[i].role+`
                                </td>
                                <td>
                                    `+response[i].email+`
                                </td>
                                <td>
                                    `+response[i].password+`
                                </td>
                                <td>
                                    `+response[i].remember_token+`
                                </td>
                                <td>
                                        <a href="{{ URL::to('/index/`+response[i].id+`/editdata') }}" class="btn btn-primary">Edit</a>

                                        <form action="`+response[i].id+`" method="POST" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button type='submit' class='btn btn-danger'>
                                                Hapus
                                            </button>
                                        </form>
                                </td>
                            </tr>
                        `);
                    });

                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });


    </script>
    @yield('scriptjs')
</body>

</html>
