<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SMKINDONESIA</title>
    <link rel="stylesheet" href="{{asset('css/materialize.min.css')}}">

     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
    <style>
        .slider h3 {
            text-shadow: 3px 3px 5px rgba(0, 0, 0, 0.5);
            font-weight: bold;

        }
        .slider h5{
            font-weight: 400;
            text-shadow: 1px 1px 5px rgba(0, 0, 0, 2);
        }

        .card .card-image img{
          height: 250px;
        }



    </style>
<body>





  <nav class="blue darken-1 ">
             <div class="nav-wrapper">
                <a href="./master" class="brand-logo center">SMK INDONESIA</a>
                    {{-- Items --}}
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li>
                          <b>Hai, {{auth()->user()->name}}</b>
                        </li>
                        <li>
                            <a href="{{URL::to('/logout')}}">
                              Logout
                            </a>
                        </li>
                    </ul>
                    <ul id="nav-mobile" class="left hide-on-med-and-down">
                        <li><a href="#" id="buttonTrigger" class="sidenav-trigger show-on-large" ><i class="material-icons">menu</i></a></li>
                        <li></li>
                    </ul>
            </div>
    </nav>

  <ul id="slide-out" class="sidenav">
    <li><div class="user-view">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#user"><img class="circle" src="images/yuna.jpg"></a>
      <a href="#name"><span class="white-text name">John Doe</span></a>
      <a href="#email"><span class="white-text email">jdandturk@gmail.com</span></a>
    </div></li>
    <li><a href="#!"><i class="material-icons">cloud</i>First Link With Icon</a></li>
    <li><a href="#!">Second Link</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Subheader</a></li>
    <li><a class="waves-effect" href="#!">Third Link With Waves</a></li>
  </ul>





        <div class="container">
            <div class="row">
                    @if (Request::path() == 'master')
                    <div class="slider">
                            <ul class="slides">

                              <li>
                                <img src="https://live.staticflickr.com/7137/27421550280_2f5fb0ab7f_b.jpg"> <!-- random image -->
                                <div class="caption left-align">
                                  <h3>SMK BISA, SMK HEBAT!</h3>
                                  <h5 class="light grey-text text-lighten-3">Orang yang berhenti belajar adalah orang yang lanjut usia, walaupun umurnya masih muda. Namun, orang yang tidak pernah berhenti belajar, maka akan selamanya menjadi pemuda
                                        – Henry Ford.</h5>
                                </div>

                            </ul>
                    </div>
                    <div class="col s6">
                            <div class="card large hoverable">
                                    <div class="card-image waves-effect waves-block waves-light">
                                      <img class="activator" src="https://www.sas.com/en_us/learn/academic-programs/students/_jcr_content/par/styledcontainer_24bc/image.img.jpg/1533556021909.jpg">
                                    </div>
                                    <div class="card-content">
                                      <span class="card-title activator grey-text text-darken-4">Menu Siswa</span>
                                      <p>Berisi data-data murid dari seluruh jurusan dan juga nilai masing-masing murid.</p><br>
                                      <p><a class="waves-effect waves-light btn-large" href="{{URL::to('siswa')}}">Pergi</a></p>
                                    </div>
                                    <div class="card-reveal">
                                      <span class="card-title grey-text text-darken-4">Definisi Siswa<i class="material-icons right">Tutup</i></span>
                                      <p>Murid biasanya digunakan untuk seseorang yang mengikuti suatu program pendidikan di sekolah atau lembaga pendidikan lainnya, di bawah bimbingan seorang atau beberapa guru. Dalam konteks keagamaan murid digunakan sebaai sebutan bagi seseorang yang mengikuti bimbingan seorang tokoh bijaksana.</p>
                                    </div>
                            </div>
                    </div>
                    <div class="col s6">
                            <div class="card large hoverable">
                                    <div class="card-image waves-effect waves-block waves-light">
                                      <img class="activator" src="https://media.gannett-cdn.com/29906170001/29906170001_5780351010001_5780346124001-vs.jpg">
                                    </div>
                                    <div class="card-content">
                                      <span class="card-title activator grey-text text-darken-4">Menu Guru</span>
                                      <p>Berisi data-data guru dan mapel atau kelas apa saja yang mereka ajari</p><br>
                                      <p><a class="waves-effect waves-light btn-large" href="{{URL::to('/guru')}}">Pergi</a></p>
                                    </div>
                                    <div class="card-reveal">
                                      <span class="card-title grey-text text-darken-4">Definisi Guru<i class="material-icons right">Tutup</i></span>
                                      <p>Guru (bahasa Sanskerta: गुरू yang berarti guru, tetapi arti secara harfiahnya adalah "berat") adalah seorang pengajar suatu ilmu. Dalam bahasa Indonesia, guru umumnya merujuk pendidik profesional dengan tugas utama mendidik, mengajar, membimbing, mengarahkan, melatih, menilai, dan mengevaluasi peserta didik</p>
                                    </div>
                            </div>
                    </div>


                    @elseif(Request::path() == 'siswa')
                    <div class="col s6">
                      @yield('form')
                    </div>

                    <div class="col s6">
                      @yield('form2')
                    </div>


                      @yield('table')

                    @elseif(Request::path() == 'guru')
                      <div class="col s12">
                        @yield('content')
                      </div>
                    @endif



            </div>

        </div>







<script src="{{asset('js/jquery-3.4.1.min.js  ')}}"></script>
<script src="{{asset('js/materialize.min.js')}}"></script>
<script type="text/javascript">


    const slider = document.querySelectorAll('.slider');
    M.Slider.init(slider,{
        indicators :false,
        transition : 600,
        interval : 3000
    });

    document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
            edge: 'left',
            draggable: true,
            inDuration: 250,
            outDuration: 200,
            onOpenStart: null,
            onOpenEnd: null,
            onCloseStart: null,
            onCloseEnd: null,
            preventScrolling: true
    });


  });



  //   document.addEventListener('DOMContentLoaded', function() {
  //   var elems = document.querySelectorAll('.modal');
  //   var instances = M.Modal.init(elems, options);
  // });


    var selectedJurusan,selectedKelas;





    $(document).ready(function(){
      $('#buttonTrigger').sidenav();

      $(".tabledata").remove();

        $("#select,#select2").change(function(){
          selectedKelas  = $("#select").children("option:selected").val();
          selectedJurusan = $("#select2").children("option: selected").val();

          $.ajax({
          url : "http://localhost/projek-basis-data/public/display/"+selectedKelas+"/"+selectedJurusan,
          type : "GET",
          success : function(result){





            var table = `<table class="tabledata striped highlight centered">
                            <thead class="center-align">
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                            </thead>
                            <tbody id="tbodyIsiData">

                            </tbody>
                        </table> `;
            $("#table").html(table);

            if(!$.trim(result)){
              $("#tbodyIsiData").append(
                    `<tr align=center>
                      <td colspan=4>Data Tidak Ada</td>
                    </tr>`
                  );
            }else{
                jQuery.each(result, function(i,v){

                  $("#tbodyIsiData").append(
                    `<tr>
                      <td>`+result[i].nis_siswa+`</td>
                      <td>`+result[i].nama_siswa+`</td>
                      <td>`+result[i].nama_kelas+`</td>
                      <td>`+result[i].nama_prodi+`</td>
                    </tr>`
                  );
              });
            }



            setTimeout(() => {
                  $(".tabledata tbody tr").click(function(){
                    $(".modal").modal();
                    $("#modal1").modal("open");
                    var nis = $(this).find("td").eq(0).html();
                    $("#txtNamaSiswa").text($(this).find("td").eq(1).html());
                    $("#tbodyIsiData2").empty();

                    $.ajax({
                       url : '{{URL::to("/getNilai")}}/'+nis,
                       type : "GET",
                       success : function(data){
                          var table2 = `<table class="striped highlight">
                                        <thead id="txtThead">

                                        </thead>
                                        <tbody id ="tbodyIsiData2">

                                        </tbody>
                                      </table>`;
                          $("#table2").html(table2);
                          $("#tbodyIsiData2").empty();
                          $.ajax({
                            url : '{{URL::to("/getNilai")}}/'+nis,
                            type : "GET",
                            success : function(data){
                              jQuery.each(data , function(i){
                                  $("#tbodyIsiData2").append(`<td>`+data[i].nilai+`</td>`);
                              });
                            }
                          });
                         jQuery.each(data, function(i){
                            $("#txtThead").append(`<th>`+data[i].nama_mapel+`</th>`);
                         });
                       }
                    });
                });
            }, 1000);



          }
        });
      });

        // $.ajax({
        //   url : "http://localhost/projek-basis-data/public/display",
        //   type : "GET",
        //   success : function(result){

        //     var table = `<table class="tabledata highlight centered">
        //                     <thead class="center-align">
        //                         <th>NIS</th>
        //                         <th>Nama</th>
        //                         <th>Kelas</th>
        //                         <th>Jurusan</th>
        //                     </thead>
        //                     <tbody id="tbodyIsiData">

        //                     </tbody>
        //                 </table> `;
        //     $("#table").append(table);

        //     jQuery.each(result, function(i,v){

        //         $("#tbodyIsiData").append(
        //           `<tr>
        //             <td class ="td-data">`+result[i].nis_siswa+`</td>
        //             <td>`+result[i].nama_siswa+`</td>
        //             <td>`+result[i].nama_kelas+`</td>
        //             <td>`+result[i].nama_prodi+`</td>
        //           </tr>`
        //         );
        //     });
        //   }
        // });


    });




</script>

</body>
</html>
