@extends('master')

@section('form')
  <select class="browser-default" style="margin-top:50px;" id="select">
    <option value="" disabled selected >Pilih Kelas</option>
    <option value="2">Kelas 10</option>
    <option value="3">Kelas 11</option>
    <option value="4">Kelas 12</option>
  </select>

@endsection

@section('form2')
  <select class="browser-default" style="margin-top:50px;" id="select2">
    <option value="" disabled selected>Pilih Jurusan</option>
    <option value="2">Rekayasa Perangkat Lunak</option>
    <option value="1">Multimedia</option>
    <option value="3">Teknik Komputer Jaringan</option>
  </select>

@endsection

@section('table')
    {{-- <table class="striped highlight">
        <thead class="center-align">
            <th>NIS</th>
            <th>Nama</th>
            <th>Kelas</th>
            <th>Jurusan</th>
        </thead>
        <tbody id="tbodyIsiData">

        </tbody>
    </table> --}}
    <br>
    <div id="table">

    </div>

    {{-- Modal --}}

    <!-- Modal Structure -->
  <div id="modal1" class="modal">
      <div class="modal-content">
        <h4 id="txtNamaSiswa">Nama Siswa</h4>
            <div id="table2">

            </div>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
      </div>
    </div>
@endsection
