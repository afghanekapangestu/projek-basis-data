@extends('index')

@section('admin_tambahdata')
<div class="panel-heading">
    <h3>Tambah Data</h3>
</div>

                            <div class="panel-body">
                                <div class="row">
                                    <form action="{{ route('index') }}"" method="POST">
                                        <div class="col-md-6">
                                            <label for="nis">Identitas</label>
                                            @csrf
                                            <input class="form-control input-lg" placeholder="Identitas" name="nis" type="text"><br>
                                            <label for="name">Nama</label>
                                            <input class="form-control input-lg" placeholder="Nama" name="name" type="text"><br>
                                            <label for="name">Role</label>
                                            <select name="role" id="role" class="form-control">
                                                <option value="admin">Admin</option>
                                                <option value="guru">Guru</option>
                                                <option value="siswa">Siswa</option>
                                            </select><br>

                                             <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim</button>
                                             <button type="reset" class="btn btn-danger"><i class="fa fa-ban"></i> Reset</button>
                                        </div>

                                        <div class="col-md-6">
                                            <label for="password">Password</label>
                                            <input class="form-control input-lg" placeholder="Password" name="password" type="password"><br>
                                            <label for="email">Email</label>
                                            <input class="form-control input-lg " placeholder="Email" name="email" type="email">
                                            <label for="jk_guru">Jenis Kelamin</label>
                                            <select name="jk" id=""></select>
                                        </div>
                                    </div>
                                </div>
                            </form>
</div>
@endsection
