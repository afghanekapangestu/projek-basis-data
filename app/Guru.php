<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    //
    protected $table = 'tabel_guru';
    public $timestamps = false;
    protected $fillable = [
        'nip_guru','nama_guru','kelas_guru'
    ];
}
