<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    //

    protected $table = 'tabel_prodi';
    public $timestamps = false;
    protected $fillable = [
        'id_prodi','nama_prodi'
    ];
}
