<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    //
    protected $table = 'tabel_kelas';
    protected $fillable = [
        'id_kelas','nama_kelas','id_prodi'
    ];
}
