<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'tabel_nilai';
    public $timestamps = false;
    protected $fillable = [
        'id_nilai','nis_siswa','nip_guru','id_mapel','uh','uts','uas','na'
    ];
}
