<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class mainController extends Controller
{
    public function siswa(){
        return view('siswa');
	}

    public function getDataSiswa($kelas,$jurusan){
    	$data = \DB::table('tabel_siswa')
    	->join('tabel_kelas','tabel_kelas.id_kelas','=','tabel_siswa.id_kelas')
    	->join('tabel_prodi','tabel_prodi.id_prodi','=','tabel_siswa.id_prodi')
		->where('tabel_kelas.id_kelas',$kelas)
		->where('tabel_prodi.id_prodi',$jurusan)
		->orderBy('nama_siswa',"ASC")
		->get();

    	return response()->json($data);
	}	
	public function getDataSiswaAll(){
    	$data = \DB::table('tabel_nilai')
    	->join('tabel_kelas','tabel_kelas.id_kelas','=','tabel_nilai.id_kelas')
    	->join('tabel_prodi','tabel_prodi.id_prodi','=','tabel_nilai.id_prodi')
		->join('tabel_siswa','tabel_siswa.nis_siswa','=','tabel_nilai.nis_siswa')
		->orderBy('nama_siswa',"ASC")
		->get();

    	return response()->json($data);
	}
	
	// public function getDataMapel(){
	// 	$data = \DB::table('tabel_mapel')
	// 	->orderBy('id_mapel',"ASC")
	// 	->get();

	// 	return response()->json($data);
	// }
	public function getDataNilai($nis){
		$data = \DB::table('tabel_nilai')
        ->join('tabel_mapel','tabel_nilai.id_mapel','=','tabel_mapel.id_mapel')
		->where('nis_siswa',$nis)
		->orderBy('tabel_mapel.id_mapel',"ASC")
		->get();

		return response()->json($data);
	}

	

}
