<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Kelas;
use App\Siswa;
use App\Prodi;
use App\Guru;
use Illuminate\Support\Str;
use App\Http\Controllers\Redirect;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    //Create
    public function create(Request $r){

        // Validation

        $rules= [
            'name' => 'required',
            'nis' => 'required|size:11',
            'password' => 'required',
            'email' => 'required',
            'role' => 'required'
        ];

        $customMessages= [
            'name.required' => 'Nama tidak boleh kosong',
            'nis.size' => 'Nomor Identitas harus 11 Karakter',
            'nis.required' => 'Nomor Identitas tidak boleh kosong',
            'password.required' => 'Password tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong'
        ];

        $this->validate($r,$rules,$customMessages);
        
        User::create([
            'name' => $r->name,
            'nis' => $r->nis,
            'password' => bcrypt($r->password),
            'email' => $r->email,
            'remember_token' => Str::random(60),
            'role' => $r->role
        ]);

        if($r->role == 'siswa'){
            Siswa::create([
                'nis_siswa' => $r->nis,
                'nama_siswa' => $r->name,
                'id_kelas' => $r->id_kelas,
                'id_prodi' => $r->id_prodi
            ]);
        }else if($r->role == 'guru'){
            $kelas_guru = $_POST['kelas_guru'];
            $value = implode(',',$kelas_guru);
            Guru::create([
                'nip_guru' => $r->nis,
                'nama_guru' => $r->name,
                'kelas_guru' => $value 
            ]);
        }
        


        return redirect("index/lihatdata");
    }
    
    public function getProdi1(){
        $data = Prodi::all();

        return response()->json($data, 200);
    }

    public function tambahdata(){
        $data = \DB::table('tabel_kelas')
                ->join('tabel_prodi','tabel_prodi.id_prodi','=','tabel_kelas.id_prodi')
                ->orderBy('nama_kelas','ASC')
                ->get();

        return view('admin_layouts.tambahdata',compact('data'));
    }

    public function getProdi($prodi){
        $data = \DB::table('tabel_kelas')
                ->join('tabel_prodi','tabel_prodi.id_prodi','=','tabel_kelas.id_prodi')
                ->orderBy('nama_kelas','ASC')
                ->where('id_kelas',$prodi)
                ->get();

        return response()->json($data, 200);
    }

    public function lihatdata($user){
        $data = User::all()->where('role',$user);

        return response()->json($data);
    }

    public function delete(User $user){
        User::destroy($user->id);

        return redirect('index/lihatdata')->with('status','Data Berhasil dihapus!');
    }

    public function edit($user){
        $data = User::where('id',$user)->get();
        return view('admin_layouts.editdata',['data' => $data]);
    }

    public function update(Request $r, User $user){
        User::where('id',$user->id)
              ->update([
                  'nis' => $r->nis,
                  'name' => $r->name,
                  'role' => $r->role,
                  'password' => bcrypt($r->password),
                  'email' => $r->email
              ]);

              if($r->role == 'siswa'){
                Siswa::where('nis_siswa',$user->id)
                ->update([     
                    'id_kelas' => $r->id_kelas,
                    'id_prodi' => $r->id_prodi
                ]);
            }else if($r->role == 'guru'){
                $kelas_guru = $_POST['kelas_guru'];
                $value = implode(',',$kelas_guru);
                Guru::where('nip_guru')->update([
                    'kelas_guru' => $value 
                ]);
            }
            
        return redirect('index/lihatdata')->with('status','Data Berhasil Edit!');
    }
}
