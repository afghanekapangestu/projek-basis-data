<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    //

    public function login(){
        return view('auth.login');
    }

    public function postlogin(Request $request){
        $credentials = $request->only('nis','password');

        if(Auth::attempt($credentials,true)){
            return redirect('/index');
        }

        return redirect('/login')->with('status','Password atau Nomor Identitas Salah');
    }

    public function logout(){
        Auth::logout();

        return redirect('/login');
    }
}
