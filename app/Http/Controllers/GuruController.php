<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Nilai;
use App\Siswa;
use App\Guru;

class GuruController extends Controller
{
    // Lihat Data

    public function lihatnilai($nip){
        $data = Nilai::where('nip_guru',$nip)->get();

        return view('guru_layouts.lihat_nilai',compact('data'));
    }

    public function createnilai(Request $r){
        $data = Nilai::create([
            "nis_siswa" => $r->nis_siswa,
            "nip_guru" => $r->nip_guru,
            "id_mapel" => $r->id_mapel,
            "uh" => $r->uh,
            "uts" => $r->uts,
            "uas" => $r->uas,
            "na" => $r->na

            
        ]);

        return redirect('index/guru/lihatnilai')->with('status','Berhasil ditambahkan!');
    }

    public function formnilai(){
        return view('guru_layouts.tambah_data');
    }

    public function getNisSiswa(){
        $data = \DB::table('tabel_siswa')
                    ->join('tabel_kelas','tabel_kelas.id_kelas','=','tabel_siswa.id_kelas')
                    ->join('tabel_prodi','tabel_prodi.id_prodi','=','tabel_siswa.id_prodi')
                    ->get();

        return response()->json($data, 200);
    }

    public function getMapel(){
        $data = \DB::table('tabel_mapel')->get();

        return response()->json($data, 200);
    }
}
