<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    //
    public $timestamps = false;
    protected $table = 'tabel_siswa';
    protected $fillable = [
        'nis_siswa','nama_siswa','id_kelas','id_prodi'
    ];
}
